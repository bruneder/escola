package br.com.bruneder.escola.business;

import br.com.bruneder.escola.model.Aluno;

import java.util.List;

public interface AlunoBusiness extends GenericBusiness<Aluno> {

    public List<Aluno> buscaPorNome(String nome);

}
