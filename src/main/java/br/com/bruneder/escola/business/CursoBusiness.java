package br.com.bruneder.escola.business;

import br.com.bruneder.escola.model.Curso;

public interface CursoBusiness extends GenericBusiness<Curso> {
}
