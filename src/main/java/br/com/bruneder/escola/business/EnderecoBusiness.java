package br.com.bruneder.escola.business;


import br.com.bruneder.escola.model.Endereco;

public interface EnderecoBusiness extends GenericBusiness<Endereco> {

}
