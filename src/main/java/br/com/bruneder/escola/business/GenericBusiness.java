package br.com.bruneder.escola.business;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GenericBusiness<T> {
    public Page<T> listarTodos(Pageable pageable);

    public T buscarPorId(Integer id);

    public T criar(T t);

    public T atualizar(T t);

    public void deletar(Integer id);

}
