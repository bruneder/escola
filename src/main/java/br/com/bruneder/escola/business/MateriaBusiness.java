package br.com.bruneder.escola.business;

import br.com.bruneder.escola.model.Materia;

public interface MateriaBusiness extends GenericBusiness<Materia> {
}
