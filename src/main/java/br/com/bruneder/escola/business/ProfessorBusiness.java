package br.com.bruneder.escola.business;

import br.com.bruneder.escola.model.Professor;

public interface ProfessorBusiness extends GenericBusiness<Professor> {
}
