package br.com.bruneder.escola.business;

import br.com.bruneder.escola.model.Turma;

public interface TurmaBusiness extends GenericBusiness<Turma> {
}
