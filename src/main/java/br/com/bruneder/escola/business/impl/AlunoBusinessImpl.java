package br.com.bruneder.escola.business.impl;

import br.com.bruneder.escola.business.AlunoBusiness;
import br.com.bruneder.escola.model.Aluno;
import br.com.bruneder.escola.repository.AlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AlunoBusinessImpl implements AlunoBusiness {

    private AlunoRepository alunoRepository;

    @Autowired
    public AlunoBusinessImpl(AlunoRepository alunoRepository) {
        this.alunoRepository = alunoRepository;
    }

    @Override
    public Page<Aluno> listarTodos(Pageable pageable) {
        return alunoRepository.findAll(pageable);
    }

    @Override
    public Aluno buscarPorId(Integer id) {
        return alunoRepository.findById(id).get();
    }

    @Override
    public Aluno criar(Aluno aluno) {
        return alunoRepository.save(aluno);
    }

    @Override
    public Aluno atualizar(Aluno aluno) {
        return alunoRepository.save(aluno);
    }

    @Override
    public void deletar(Integer id) {
        alunoRepository.deleteById(id);
    }

    @Override
    public List<Aluno> buscaPorNome(String nome) {
        return alunoRepository.findAllByNomeContaining(nome);
    }
}
