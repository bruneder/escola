package br.com.bruneder.escola.business.impl;

import br.com.bruneder.escola.business.CursoBusiness;
import br.com.bruneder.escola.model.Curso;
import br.com.bruneder.escola.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CursoBusinesslmpl implements CursoBusiness {

    private CursoRepository cursoRepository;

    @Autowired
    public CursoBusinesslmpl(CursoRepository cursoRepository) {
        this.cursoRepository = cursoRepository;
    }

    @Override
    public Page<Curso> listarTodos(Pageable pageable) {
        return cursoRepository.findAll(pageable);
    }

    @Override
    public Curso buscarPorId(Integer id) {
        return cursoRepository.findById(id).get();
    }

    @Override
    public Curso criar(Curso curso) {
        return cursoRepository.save(curso);
    }

    @Override
    public Curso atualizar(Curso curso) {
        return cursoRepository.save(curso);
    }

    @Override
    public void deletar(Integer id) {
        cursoRepository.deleteById(id);
    }
}
