package br.com.bruneder.escola.business.impl;

import br.com.bruneder.escola.business.EnderecoBusiness;
import br.com.bruneder.escola.model.Endereco;
import br.com.bruneder.escola.repository.EnderecoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EnderecoBusinessImpl implements EnderecoBusiness {

    EnderecoRepository enderecoRepository;

    @Autowired
    public EnderecoBusinessImpl(EnderecoRepository enderecoRepository) {
        this.enderecoRepository = enderecoRepository;
    }

    @Override
    public Page<Endereco> listarTodos(Pageable pageable) {
        return enderecoRepository.findAll(pageable);
    }

    @Override
    public Endereco buscarPorId(Integer id) {
        return enderecoRepository.findById(id).get();
    }

    @Override
    public Endereco criar(Endereco endereco) {
        return enderecoRepository.save(endereco);
    }

    @Override
    public Endereco atualizar(Endereco endereco) {
        return enderecoRepository.save(endereco);
    }

    @Override
    public void deletar(Integer id) {
        enderecoRepository.deleteById(id);
    }
}
