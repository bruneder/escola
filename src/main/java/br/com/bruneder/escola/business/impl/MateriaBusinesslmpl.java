package br.com.bruneder.escola.business.impl;

import br.com.bruneder.escola.business.MateriaBusiness;
import br.com.bruneder.escola.model.Materia;
import br.com.bruneder.escola.repository.MateriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MateriaBusinesslmpl implements MateriaBusiness {

    private MateriaRepository materiaRepository;

    @Autowired
    public MateriaBusinesslmpl(MateriaRepository materiaRepository) {
        this.materiaRepository = materiaRepository;
    }

    @Override
    public Page<Materia> listarTodos(Pageable pageable) {
        return materiaRepository.findAll(pageable);
    }

    @Override
    public Materia buscarPorId(Integer id) {
        return materiaRepository.findById(id).get();
    }

    @Override
    public Materia criar(Materia materia) {
        return materiaRepository.save(materia);
    }

    @Override
    public Materia atualizar(Materia materia) {
        return materiaRepository.save(materia);
    }

    @Override
    public void deletar(Integer id) {
        materiaRepository.deleteById(id);
    }
}
