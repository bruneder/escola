package br.com.bruneder.escola.business.impl;

import br.com.bruneder.escola.business.ProfessorBusiness;
import br.com.bruneder.escola.model.Professor;
import br.com.bruneder.escola.repository.ProfessorReposiroty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProfessorBusinessImpl implements ProfessorBusiness {

    ProfessorReposiroty professorReposiroty;

    @Autowired
    public ProfessorBusinessImpl(ProfessorReposiroty professorReposiroty) {
        this.professorReposiroty = professorReposiroty;
    }

    @Override
    public Page<Professor> listarTodos(Pageable pageable) {
        return professorReposiroty.findAll(pageable);
    }

    @Override
    public Professor buscarPorId(Integer id) {
        return professorReposiroty.findById(id).get();
    }

    @Override
    public Professor criar(Professor professor) {
        return professorReposiroty.save(professor);
    }

    @Override
    public Professor atualizar(Professor professor) {
        return professorReposiroty.save(professor);
    }

    @Override
    public void deletar(Integer id) {
        professorReposiroty.deleteById(id);
    }
}
