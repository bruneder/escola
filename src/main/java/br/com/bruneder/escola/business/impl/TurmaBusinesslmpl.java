package br.com.bruneder.escola.business.impl;

import br.com.bruneder.escola.business.TurmaBusiness;
import br.com.bruneder.escola.model.Turma;
import br.com.bruneder.escola.repository.TurmaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TurmaBusinesslmpl implements TurmaBusiness {

    private TurmaRepository turmaRepository;

    @Autowired
    public TurmaBusinesslmpl(TurmaRepository turmaRepository) {
        this.turmaRepository = turmaRepository;
    }

    @Override
    public Page<Turma> listarTodos(Pageable pageable) {
        return turmaRepository.findAll(pageable);
    }

    @Override
    public Turma buscarPorId(Integer id) {
        return turmaRepository.findById(id).get();
    }

    @Override
    public Turma criar(Turma turma) {
        return turmaRepository.save(turma);
    }

    @Override
    public Turma atualizar(Turma turma) {
        return turmaRepository.save(turma);
    }

    @Override
    public void deletar(Integer id) {
        turmaRepository.deleteById(id);
    }
}
