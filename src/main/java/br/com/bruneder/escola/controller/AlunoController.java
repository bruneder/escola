package br.com.bruneder.escola.controller;

import br.com.bruneder.escola.business.AlunoBusiness;
import br.com.bruneder.escola.model.Aluno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/v1/alunos")
public class AlunoController {

    private AlunoBusiness alunoBusiness;

    @Autowired
    public AlunoController(AlunoBusiness alunoBusiness) {
        this.alunoBusiness = alunoBusiness;
    }

    @GetMapping
    public ResponseEntity<Page<Aluno>> listarTodos(
            @SortDefault.SortDefaults({@SortDefault(sort = "nome", direction = Sort.Direction.ASC)})
                    Pageable pageable) {
        return ResponseEntity.ok().body(alunoBusiness.listarTodos(pageable));
    }

    @PostMapping
    public ResponseEntity<Aluno> criar(@RequestBody Aluno aluno) {
        return ResponseEntity.status(HttpStatus.CREATED).body(alunoBusiness.criar(aluno));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Aluno> buscarPorId(@PathVariable int id) {
        return ResponseEntity.status(HttpStatus.OK).body(alunoBusiness.buscarPorId(id));
    }

    @PutMapping
    public ResponseEntity<Aluno> atualizar(@PathVariable(value = "id") int id, @RequestBody Aluno alunoParamentro) {

        Aluno alunoAtual = alunoBusiness.buscarPorId(id);

        alunoAtual.setMatricula(alunoParamentro.getMatricula());
        alunoAtual.setTurma(alunoParamentro.getTurma());
        alunoAtual.setCpf(alunoParamentro.getCpf());
        alunoAtual.setEndereco(alunoParamentro.getEndereco());
        alunoAtual.setNome(alunoParamentro.getNome());

        Aluno alunoSalvar = alunoBusiness.atualizar(alunoAtual);

        return ResponseEntity.ok(alunoSalvar);
    }

    @RequestMapping("/nome/{nome}")
    public ResponseEntity<List<Aluno>> buscarPorNome(@PathVariable String nome) {
        return ResponseEntity.status(HttpStatus.OK).body(alunoBusiness.buscaPorNome(nome));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deletar(@PathVariable(value = "id") int id) {
        alunoBusiness.deletar(id);
        return ResponseEntity.ok().body("O aluno foi deletado com sucesso");
    }

}
