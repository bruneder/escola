package br.com.bruneder.escola.controller;

import br.com.bruneder.escola.business.CursoBusiness;
import br.com.bruneder.escola.model.Curso;
import br.com.bruneder.escola.model.Materia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/cursos")
public class CursoController {

    private CursoBusiness cursoBusiness;

    @Autowired
    public CursoController(CursoBusiness cursoBusiness) {
        this.cursoBusiness = cursoBusiness;
    }

    @GetMapping()
    public ResponseEntity<Page<Curso>> listarTodos(Pageable pageable) {
        return ResponseEntity.ok().body(cursoBusiness.listarTodos(pageable));
    }

    @PostMapping()
    public ResponseEntity<Curso> criar(@RequestBody Curso curso) {
        return ResponseEntity.status(HttpStatus.CREATED).body(cursoBusiness.criar(curso));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Curso> buscarPorId(@PathVariable int id) {
        return ResponseEntity.status(HttpStatus.OK).body(cursoBusiness.buscarPorId(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Curso> atualizar(@PathVariable(value = "id") int id, @RequestBody Curso cursoParamentro) {

        Curso cursoAtual = cursoBusiness.buscarPorId(id);

        cursoAtual.setNome(cursoParamentro.getNome());

        Curso cursoSalvar = cursoBusiness.atualizar(cursoAtual);
        return ResponseEntity.ok(cursoSalvar);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deletar(@PathVariable(value = "id") int id) {

        cursoBusiness.deletar(id);
        return ResponseEntity.ok().body("O curso foi excluido com sucesso");
    }
}
