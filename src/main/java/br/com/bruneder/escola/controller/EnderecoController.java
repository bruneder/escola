package br.com.bruneder.escola.controller;

import br.com.bruneder.escola.business.EnderecoBusiness;
import br.com.bruneder.escola.model.Endereco;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/enderecos")
public class EnderecoController {

    private EnderecoBusiness enderecoBusiness;

    @Autowired
    public EnderecoController(EnderecoBusiness enderecoBusiness) {
        this.enderecoBusiness = enderecoBusiness;
    }

    @GetMapping()
    public ResponseEntity<Page<Endereco>> listarTodos(Pageable pageable) {
        return ResponseEntity.ok().body(enderecoBusiness.listarTodos(pageable));
    }

    @PostMapping()
    public ResponseEntity<Endereco> criar(@RequestBody Endereco endereco) {
        return ResponseEntity.status(HttpStatus.CREATED).body(enderecoBusiness.criar(endereco));
    }

    @GetMapping("{id}")
    public ResponseEntity<Endereco> buscarPorId(@PathVariable int id) {
        return ResponseEntity.status(HttpStatus.OK).body(enderecoBusiness.buscarPorId(id));
    }

    @PutMapping
    public ResponseEntity<Endereco> atualizar(@PathVariable(value = "id") int id, @RequestBody Endereco enderecoParametro) {

        Endereco enderecoAtual = enderecoBusiness.buscarPorId(id);

        enderecoAtual.setBairro(enderecoParametro.getBairro());
        enderecoAtual.setCidade(enderecoParametro.getCidade());
        enderecoAtual.setLogradouro(enderecoParametro.getLogradouro());
        enderecoAtual.setNumero(enderecoParametro.getNumero());
        enderecoAtual.setUf(enderecoParametro.getUf());

        Endereco enderecoSalvar = enderecoBusiness.atualizar(enderecoAtual);

        return ResponseEntity.ok(enderecoSalvar);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deletar(@PathVariable(value = "id") int id) {
        enderecoBusiness.deletar(id);
        return ResponseEntity.ok().body("O endereço foi deletado com sucesso");
    }
}
