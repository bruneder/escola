package br.com.bruneder.escola.controller;

import br.com.bruneder.escola.business.MateriaBusiness;
import br.com.bruneder.escola.model.Materia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/materias")
public class MateriaController {

    private MateriaBusiness materiaBusiness;

    @Autowired
    public MateriaController(MateriaBusiness materiaBusiness) {
        this.materiaBusiness = materiaBusiness;
    }

    @GetMapping()
    public ResponseEntity<Page<Materia>> ListarTodos(Pageable pageable) {
        return ResponseEntity.ok().body(materiaBusiness.listarTodos(pageable));
    }

    @PostMapping()
    public ResponseEntity<Materia> criar(@RequestBody Materia materia) {
        return ResponseEntity.status(HttpStatus.CREATED).body(materiaBusiness.criar(materia));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Materia> buscarPorId(@PathVariable int id) {
        return ResponseEntity.status(HttpStatus.OK).body(materiaBusiness.buscarPorId(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Materia> atualizar(@PathVariable(value = "id") int id, @RequestBody Materia materiaParamentro) {

        Materia materiaAtual = materiaBusiness.buscarPorId(id);
        materiaAtual.setNome(materiaParamentro.getNome());

        Materia materiaSalvar = materiaBusiness.atualizar(materiaAtual);

        return ResponseEntity.ok(materiaSalvar);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deletar(@PathVariable(value = "id") int id) {

        materiaBusiness.deletar(id);
        return ResponseEntity.ok().body("O material foi excluido com sucesso");
    }

}
