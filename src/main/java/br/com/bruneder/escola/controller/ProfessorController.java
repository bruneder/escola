package br.com.bruneder.escola.controller;

import br.com.bruneder.escola.business.ProfessorBusiness;
import br.com.bruneder.escola.model.Endereco;
import br.com.bruneder.escola.model.Professor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/professores")
public class ProfessorController {

    private ProfessorBusiness professorBusiness;

    @Autowired
    public ProfessorController(ProfessorBusiness professorBusiness) {
        this.professorBusiness = professorBusiness;
    }

    @GetMapping()
    public ResponseEntity<Page<Professor>> listarTodos(Pageable pageable) {
        return ResponseEntity.ok().body(professorBusiness.listarTodos(pageable));
    }

    @PostMapping()
    public ResponseEntity<Professor> criar(@RequestBody Professor professor) {
        return ResponseEntity.status(HttpStatus.CREATED).body(professorBusiness.criar(professor));
    }

    @GetMapping("{id}")
    public ResponseEntity<Professor> buscarPorId(@PathVariable int id) {
        return ResponseEntity.status(HttpStatus.OK).body(professorBusiness.buscarPorId(id));
    }

    @PutMapping
    public ResponseEntity<Professor> atualizar(@PathVariable(value = "id") int id, @RequestBody Professor professorParametro) {

        Professor professorAtual = professorBusiness.buscarPorId(id);

        professorAtual.setNome(professorParametro.getNome());
        professorAtual.setRegistro(professorParametro.getRegistro());
        professorAtual.setCpf(professorParametro.getCpf());
        professorAtual.setEndereco(professorParametro.getEndereco());


        Professor  professorSalvar = professorBusiness.atualizar(professorAtual);

        return ResponseEntity.ok(professorSalvar);

    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deletar(@PathVariable(value = "id") int id) {

        professorBusiness.deletar(id);

        return ResponseEntity.ok().body("O professor foi deletado com sucesso");

    }
}
