package br.com.bruneder.escola.controller;

import br.com.bruneder.escola.business.TurmaBusiness;
import br.com.bruneder.escola.model.Turma;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/turmas")
public class TurmaController {
    private TurmaBusiness turmaBusiness;

    @Autowired
    public TurmaController(TurmaBusiness turmaBusiness) {
        this.turmaBusiness = turmaBusiness;
    }

    @GetMapping
    public ResponseEntity<Page<Turma>> listarTodos(Pageable pageable) {
        return ResponseEntity.ok().body(turmaBusiness.listarTodos(pageable));
    }

    @PostMapping
    public ResponseEntity<Turma> criar(@RequestBody Turma turma) {
        return ResponseEntity.status(HttpStatus.CREATED).body(turmaBusiness.criar(turma));
    }

    @GetMapping("{id}")
    public ResponseEntity<Turma> buscarPorId(@PathVariable Integer id) {
        return ResponseEntity.status(HttpStatus.FOUND).body(turmaBusiness.buscarPorId(id));
    }

    @PutMapping("{id}")
    public ResponseEntity<Turma> atualizar(@PathVariable(value = "id") int id, @RequestBody Turma turmaParametros) {

        Turma turmaAtual = turmaBusiness.buscarPorId(id);

        turmaAtual.setAlunos(turmaParametros.getAlunos());
        turmaAtual.setCurso(turmaParametros.getCurso());
        turmaAtual.setNome(turmaParametros.getNome());

        Turma turmaSalvar = turmaBusiness.atualizar(turmaAtual);

        return ResponseEntity.ok(turmaSalvar);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deletar(@PathVariable(value = "id") int id) {

        turmaBusiness.deletar(id);
        return ResponseEntity.ok().body("A turma foi deletada com sucesso");
    }

}
