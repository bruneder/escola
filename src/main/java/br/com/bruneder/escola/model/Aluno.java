package br.com.bruneder.escola.model;

import javax.persistence.*;

@Entity
@Table(name = "alunos")
@PrimaryKeyJoinColumn(name = "id")
public class Aluno extends Pessoa {

    @Column(length = 100)
    private String matricula;

    @ManyToOne
    private Turma turma = new Turma();

    @Deprecated
    public Aluno() {
    }

    public Aluno(Integer id, String nome, String cpf, Endereco endereco, String matricula, Turma turma) {
        super(id, nome, cpf, endereco);
        this.matricula = matricula;
        this.turma = turma;
    }


    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Turma getTurma() {
        return turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }
}
