package br.com.bruneder.escola.model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "professores")
@PrimaryKeyJoinColumn(name = "id")
public class Professor extends Pessoa {

    private String registro;

    @Deprecated
    public Professor() {
    }

    public Professor(Integer id, String nome, String cpf, Endereco endereco, String registro) {
        super(id, nome, cpf, endereco);
        this.registro = registro;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }
}
