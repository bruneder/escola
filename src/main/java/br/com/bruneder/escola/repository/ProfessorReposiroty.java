package br.com.bruneder.escola.repository;

import br.com.bruneder.escola.model.Professor;
import com.sun.xml.bind.v2.model.core.ID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfessorReposiroty extends JpaRepository<Professor, Integer> {
}
